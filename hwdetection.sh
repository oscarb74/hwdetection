#!/bin/bash

############################################################################
#title           : hwdetection
#description     : detecta el hw de un pc y comprueba si ha cambiado
#author          : Óscar Borrás
#email           : oscarborras@iesjulioverne.es
#date            : 2016-07-29
#version         : 0.6
#license         : GNU GPLv3 
############################################################################



############################################################################
#  INSTRUCCIONES
############################################################################
# Se debe tener instalado: sqlite3, mailutils


############################################################################
#  FALLOS POR CORREGIR
############################################################################
#
# - config envio de email por smtp del julioverne

############################################################################
#  FUNCIONES PENDIENTES DE IMPLEMENTAR
############################################################################
# 1.- Automatizar el inicio de este script al arrancar el PC.
#
# - Envio del email que avisa a los profesores (hacerlo instalando servidor email???)
# - Generación de logs para cada proceso realizado
# - Fijar una ruta mas segura para los ficheros de config .cfg
# - Actualizar BBDD sqlite sincronizando con el original o fuente mediante git 


############################################################################
# LISTA DE ESTADOS DE EXIT:
############################################################################
# 0   - Ok
# 1	  - Se ha realizado la instalación

############################################################################
# Inicialización de variables configurables
############################################################################
#Vbles generales
VERSION="0.6"
SCRIPT_NAME="hwdetection"
LOG="$SCRIPT_NAME.log"	#Fichero log del programa
TTY_SALIDA=`tty` #Poner /dev/null si no desea salida en una terminal. La terminal debe existir
FECHA_LOG=`date +"%Y-%m-%d_%H:%M:%S"`;

#Vbles App
MEMORIA="nd"
DISCO="nd"
AVISO=false
PC_NAME=`hostname`

BBDD=$(dirname $0)/hwdetection.db

#Vbles Repo
REPO_NAME="hwdetection" #nombre repo en github
FILE_LASTUPDATE="./$REPO_NAME.date"

#variables Email
FROM_EMAIL="alertas@iesjulioverne.es"
#DESTINATARIO_EMAIL="oscarborras@iesjulioverne.es,mantenimiento@iesjulioverne.es,oscarb74@gmail.com"
DESTINATARIO_EMAIL="oscarb74@gmail.com" 
#source ./$REPO_NAME.cfg 


############################################################################
# BLOQUE FUNCIONES
############################################################################

# Función que actualiza el repositorio a la ultima versión
# Uso: actualizar_repo
# return=1 --> no se ha realizado la clonación satisfactoriamente
function actualizar_repo(){
	echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Haciendo pull:" | tee "$TTY_SALIDA" >> $LOG
	git pull | tee "$TTY_SALIDA" >> $LOG
	
	#git fetch origin
	#git reset --hard origin/master

	#si se clona actualizar fichero con fecha de hoy
	date +%Y%m%d > $FILE_LASTUPDATE 
	
#	echo "0" #return de la función
}

# Función que clona el repositorio en local. Se debe realizar solo la primera vez
function instalar(){
	git clone  https://oscarb74@bitbucket.org/oscarb74/"$REPO_NAME".git
	
	#si se clona actualizar fichero con fecha de hoy
	if [ $? -eq 0 ]; then
		date +%Y%m%d > "$REPO_NAME"/$FILE_LASTUPDATE 
	fi
	
	#eliminamos el script inicial usado para la instalación o clonación inicial
	rm $0
}

# Función que envia email de aviso
# Uso: enviar_email <destinatario> <asunto> <msg>
enviar_email(){
	echo -e "$3" | mail -s "$2" -r "$FROM_EMAIL" "$1"

	if [ $? -eq 0 ]; then
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Email enviado satisfactoriamente." | tee "$TTY_SALIDA" >> $LOG
	else
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Alerta] Error al enviar email." | tee "$TTY_SALIDA" >> $LOG
	fi
}

# Función que obtiene los datos del PC en la BBDD
function obtener_hw_BBDD() {
	local sql="select memoria,disco from hw where pc='$1'"
	local row=`sqlite3 $BBDD "${sql}"`
	echo $row	
}

# Función que averigua el hardware de un pc
# Uso:
function obtener_hw_pc(){
	#memoria
	#memoria_actual=`free -h  | grep -i Mem | awk {'print $2'}`
	MEMORIA=`cat /proc/meminfo | grep -i memtotal | awk {'print $2 " " $3'}`
	#echo "Memoria instalada: $MEMORIA" #codigo depuracion

	#disco duro
	DISCO=`lsblk -fm /dev/sda | grep -w sda | awk {'print $3'}`
	#echo "Disco instalado: $DISCO" #codigo depuracion
}

# Función que averigua el hardware de un pc
# Uso:
function comprobar_hw_pc(){

	obtener_hw_pc

	datos=$(obtener_hw_BBDD $HOSTNAME)
	memoria_BBDD=`echo $datos | awk -F'|' '{print $1}'`
	disco_BBDD=`echo $datos | awk -F'|' '{print $2}'`
	#echo "Memoria en BBDD: $memoria_BBDD" #codigo depuracion	
	#echo "Disco en BBDD: $disco_BBDD" #codigo depuracion

	#Si hay modificaciones en el hardware avisar por email. Si falla el email avisar a un equipo de profesor con netcat o similar y registrarlo en el equipo.
	msg="AVISO DE CAMBIO DE HARDWARE"
	msg="$msg\n---------------------------------------------------------------"
	msg="$msg\nPC: $PC_NAME"
	msg="$msg\n---------------------------------------------------------------"
	msg="$msg\nFecha y hora detección: `date +"%Y-%m-%d_%H:%M:%S"`"
	if [[ $MEMORIA != $memoria_BBDD ]]; then
		msg="$msg\nMemoria actual: $MEMORIA\nMemoria BBDD: $memoria_BBDD"
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Alerta] Memoria actual: $MEMORIA ** Memoria BBDD: $memoria_BBDD" | tee "$TTY_SALIDA" >> $LOG
		#enviar_email $msg
		#echo -e $msg
		AVISO=true
	fi
	
	if [[ $DISCO != $disco_BBDD ]]; then
		msg="$msg\nDisco actual: $DISCO\nDisco BBDD: $disco_BBDD"
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Alerta] Disco actual: $DISCO ** Disco BBDD: $disco_BBDD" | tee "$TTY_SALIDA" >> $LOG
		#enviar_email $msg
		AVISO=true
	fi
	
	if [[ $AVISO == "true" ]]; then
		asunto="Alerta: cambio de Hardware en PC \"$PC_NAME\""
		enviar_email "$DESTINATARIO_EMAIL" "$asunto" "$msg"
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Alerta] ¡¡¡Hardware cambiado!!!" | tee "$TTY_SALIDA" >> $LOG
	else
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] No hay cambio de hardware." | tee "$TTY_SALIDA" >> $LOG
	fi

}

# función de ayuda
function ayuda() {
cat << DESCRIPCION_AYUDA
SYNOPSIS
    $0 [-h -v]
DESCRIPCION
    Monitorizar el hardware del equipo
CODIGOS DE RETORNO
    0 Si no hay ningún error
DESCRIPCION_AYUDA
}

function version() {
	echo "$0 versión $VERSION" 
	echo
}

function comprobar_actualizaciones(){
	echo "------------------------------------------------------------------------" | tee "$TTY_SALIDA" >> $LOG
	echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Iniciando autoupdate ..." | tee "$TTY_SALIDA" >> $LOG
	if [ -f $FILE_LASTUPDATE ]  
	then
		fecha_lastbackup=`cat $FILE_LASTUPDATE`  #fecha ultima clonacion
		fecha_actual=`date +%Y%m%d`
		if [ $fecha_lastbackup != $fecha_actual ]
		then
			actualizar_repo
		else
			echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Ya se ha comprobado hoy las actualizaciones." | tee "$TTY_SALIDA" >> $LOG
		fi
	else
		actualizar_repo
	fi
	echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Finalizado autoupdate." | tee "$TTY_SALIDA" >> $LOG
	echo | tee "$TTY_SALIDA" >> $LOG
}

function Main(){
	logger -p user.info -t hwdetection "*** Iniciado script HWdetection. ***"
	echo "******************************************************************" | tee "$TTY_SALIDA" >> $LOG
	echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Iniciando HWdetection ..." | tee "$TTY_SALIDA" >> $LOG
	
	#buscar_actualizaciones
	
	comprobar_hw_pc
	
	logger -p user.info -t backup_HTPC "script HWdetection Finalizado."
	echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Finalizado HWdetection." | tee "$TTY_SALIDA" >> $LOG
	echo | tee "$TTY_SALIDA" >> $LOG
}

############################################################################
# FIN BLOQUE FUNCIONES
############################################################################

#para permitir que se cargue el sistema completo hacemos una pausa
#sleep 1m

#if [[ $EUID -ne 0 ]]; then
#	echo "Este script debe ser ejecutado por el usuario root" > $TTY_SALIDA
#	logger -p user.err -t backup_diskusb_IES "Script NO ejecutado como root"
#	exit 1
#fi

if [ $# -gt 0 ]; then
	case $1 in
		"-h"|"--help")
			ayuda
			exit 0 ;;
		"-v"|"--version")
			version
			exit 0 ;;
		*)
			echo "Parámetro '$1' incorrecto."
			ayuda
			exit 0 ;;
	esac
fi

#Llamada a la función Main que inicia el script
#comprobamos si debemos instalar el programa (hacer clonacion)
if [ ! -d .git ]  
then
	instalar
	exit 1
fi

comprobar_actualizaciones
Main
